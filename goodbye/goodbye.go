package goodbye

import "fmt"

// Hello returns a greeting for the named person.
func Bye(name string) string {
    message := fmt.Sprintf("... and goodby %v.", name)
    return message
}