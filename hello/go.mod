module fpotter.com/hello

go 1.22.4

replace fpotter.com/greetings => ../greetings

replace fpotter.com/goodbye => ../goodbye

// fpotter.com/goodbye v0.0.0-00010101000000-000000000000
require fpotter.com/greetings v0.0.0-00010101000000-000000000000

require fpotter.com/goodbye v0.0.0-00010101000000-000000000000 // indirect
