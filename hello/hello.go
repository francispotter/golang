package main

import (
    "fmt"

    "fpotter.com/greetings"
	// "fpotter.com/goodbye"
)

func main() {
    // Get a greeting message and print it.
    happy := greetings.Hello("Amor")
    fmt.Println(happy)

	// sad := goodbye.Bye("Amor")
    // fmt.Println(sad)
}
