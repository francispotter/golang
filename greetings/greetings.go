package greetings

import (
    "fmt"
	"fpotter.com/goodbye"
)

func Hello(name string) string {
    first := fmt.Sprintf("Hi, %v. Welcome!", name)
	second := goodbye.Bye(name)
	// second := " I love you!"
    return first + second
}